local M = {}

-- Function to extract top-level function definitions
local function extract_functions()
  local lines = vim.api.nvim_buf_get_lines(0, 0, -1, false)
  local functions = {}
  local line_numbers = {}
  local current_function = {}
  local start_line = 0

  for i, line in ipairs(lines) do
    if line:match("^def%s+") then
      start_line = i
      -- Create a new string with "def" removed
      local trimmed_line = line:gsub("^def%s+", "")
      -- Then remove everything after the first "(" until the end of the line
      trimmed_line = trimmed_line:gsub("%(.*$", "")
      table.insert(current_function, trimmed_line)
      table.insert(functions, table.concat(current_function, "\n"))
      table.insert(line_numbers, start_line)
      current_function = {}
    end
  end

  return functions, line_numbers
end

-- Function to create a new window and display functions
local function display_functions(functions, line_numbers)
  -- Create a new vertical split
  vim.cmd('vsplit')
  -- Move to the new window
  vim.cmd('wincmd l')

  -- Create a new buffer
  local buf = vim.api.nvim_create_buf(false, true)
  vim.api.nvim_win_set_buf(0, buf)

  -- Set buffer name
  vim.api.nvim_buf_set_name(buf, 'Python Functions')

  -- Set buffer options
  vim.api.nvim_buf_set_option(buf, 'buftype', 'nofile')
  vim.api.nvim_buf_set_option(buf, 'swapfile', false)
  vim.api.nvim_buf_set_option(buf, 'bufhidden', 'wipe')

  -- Write functions to the buffer
  local all_lines = {}
  for _, func in ipairs(functions) do
    for line in func:gmatch("[^\r\n]+") do
      table.insert(all_lines, line)
    end
    table.insert(all_lines, "") -- Add an empty line between functions
  end
  vim.api.nvim_buf_set_lines(buf, 0, -1, false, all_lines)

  -- Set buffer to read-only
  vim.api.nvim_buf_set_option(buf, 'modifiable', false)

  -- Set filetype to Python for syntax highlighting
  vim.api.nvim_buf_set_option(buf, 'filetype', 'python')

  -- Store line numbers in buffer variables
  for i, line_num in ipairs(line_numbers) do
    vim.api.nvim_buf_set_var(buf, 'line_' .. i, line_num)
  end

  -- Set up keybinding for jumping to function definition
  vim.api.nvim_buf_set_keymap(buf, 'n', 'gd', ':lua require("asras.view_funs").jump_to_definition()<CR>',
    { noremap = true, silent = true })
end

-- Function to jump to the original function definition
function M.jump_to_definition()
  local current_line = vim.api.nvim_win_get_cursor(0)[1]
  local function_index = 1
  local lines_count = 0

  -- Find the correct function index
  while lines_count < current_line do
    local func_lines = vim.split(vim.api.nvim_buf_get_lines(0, lines_count, -1, false)[1], "\n", true)
    lines_count = lines_count + #func_lines + 1 -- +1 for the empty line between functions
    if lines_count >= current_line then
      break
    end
    function_index = function_index + 1
  end

  local original_line = vim.api.nvim_buf_get_var(0, 'line_' .. function_index)

  -- Switch to the previous window (original file)
  vim.cmd('wincmd h')

  -- Move cursor to the function definition
  vim.api.nvim_win_set_cursor(0, { original_line, 0 })
end

-- Main function to be called by the plugin
function M.view_functions()
  -- Check if the current file is a Python file
  if vim.bo.filetype ~= 'python' then
    print("This plugin only works with Python files.")
    return
  end

  local functions, line_numbers = extract_functions()
  if #functions > 0 then
    display_functions(functions, line_numbers)
  else
    print("No top-level functions found in the current file.")
  end
end

return M
