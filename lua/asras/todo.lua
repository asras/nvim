function EditTodoInFloatingWindow()
  -- Check if a buffer named "todo" already exists
  local existing_bufnr = vim.fn.bufnr("todo")
  local buf

  if existing_bufnr ~= -1 then
    -- If the buffer exists, use it
    buf = existing_bufnr
    -- Ensure the buffer is loaded
    if not vim.api.nvim_buf_is_loaded(buf) then
      vim.fn.bufload(buf)
    end
  else
    -- If the buffer doesn't exist, create a new one
    buf = vim.api.nvim_create_buf(false, false)
    vim.api.nvim_buf_set_name(buf, "todo")
  end

  -- Define the dimensions and position of the floating window
  local width = 60
  local height = 20
  local row = math.floor((vim.o.lines - height) / 2)
  local col = math.floor((vim.o.columns - width) / 2)

  -- Set up the floating window options
  local opts = {
    relative = 'editor',
    width = width,
    height = height,
    row = row,
    col = col,
    style = 'minimal',
    border = 'rounded'
  }

  -- Create the floating window
  local win = vim.api.nvim_open_win(buf, true, opts)

  -- If it's a new buffer, try to read the contents of the todo file
  if existing_bufnr == -1 then
    local lines = {}
    local file = io.open("todo", "r")
    if file then
      for line in file:lines() do
        table.insert(lines, line)
      end
      file:close()
      vim.api.nvim_buf_set_lines(buf, 0, -1, false, lines)
    end
  end

  -- Set the buffer as modifiable
  vim.api.nvim_buf_set_option(buf, 'modifiable', true)

  -- Set the buffer filetype to 'text'
  vim.api.nvim_buf_set_option(buf, 'filetype', 'text')

  -- Create a user command to force writing
  vim.api.nvim_buf_create_user_command(buf, 'W', 'write!', {})

  -- Quit by pressing 'q'
  vim.api.nvim_buf_set_keymap(buf, 'n', 'q', '', {
    noremap = true,
    silent = true,
    callback = function()
      vim.api.nvim_win_close(win, true)
    end
  })

  -- Mark item done by pressing 'Ctrl-d'
  vim.api.nvim_buf_set_keymap(buf, 'n', '<C-D>', '', {
    noremap = true,
    silent = true,
    callback = function()
      local line = vim.api.nvim_get_current_line()
      if line:match("^TODO") then
        -- Replace TODO with DONE
        local result = line:gsub("TODO", "DONE")
        vim.api.nvim_set_current_line(result)
      end
    end
  })
  -- Set up an autocommand to save the file when the buffer is closed
  vim.api.nvim_create_autocmd({"BufLeave", "WinLeave"}, {
    buffer = buf,
    callback = function()
      vim.api.nvim_buf_call(buf, function()
        vim.cmd("silent! write!")
      end)
    end
  })
end

return {
  EditTodoInFloatingWindow = EditTodoInFloatingWindow,
}
