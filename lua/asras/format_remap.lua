local function format_command()
    if vim.bo.filetype == "zig" then
        local filename = vim.fn.expand("%:p")
        vim.cmd("write")
        vim.cmd("silent !zig fmt " .. filename)
        -- Update buffer
        vim.cmd("edit!")
    elseif vim.bo.filetype == "rust" then
        vim.cmd("write")
        vim.cmd("silent !cargo fmt")
        -- Update buffer
        vim.cmd("edit!")
    elseif vim.bo.filetype == "typescript" or vim.bo.filetype == "typescriptreact" then
        vim.cmd("write")
        vim.cmd("silent !npx prettier --write " .. "\"" .. vim.fn.expand("%:p") .. "\"")
        vim.cmd("edit!")
    else
        vim.lsp.buf.format()
    end
end

vim.keymap.set("n", "<leader>f", format_command)
