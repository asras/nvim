return {
    {
        "nvim-lua/plenary.nvim",
    },

    "eandrju/cellular-automaton.nvim", -- Run :lua require("cellular-automaton").start_animation("make_it_rain")
}
