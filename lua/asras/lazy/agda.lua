return {
  "isovector/cornelis",

  build = "stack build",

  dependencies = {
    "kana/vim-textobj-user",
    "neovimhaskell/nvim-hs.vim",
  },


  config = function()
    -- local set_agda_kbs = function()
      -- vim.keymap.set("n", "<leader>l", "<cmd>CornelisLoad<CR>")
      -- vim.keymap.set("n", "<leader>r", "<cmd>CornelisRefine<CR>")
      -- vim.keymap.set("n", "<leader>d", "<cmd>CornelisMakeCase<CR>")
      -- vim.keymap.set("n", "<leader>,", "<cmd>CornelisTypeContext<CR>")
      -- vim.keymap.set("n", "<leader>.", "<cmd>CornelisTypeContextInfer<CR>")
      -- vim.keymap.set("n", "<leader>n", "<cmd>CornelisSolve<CR>")
      -- vim.keymap.set("n", "<leader>a", "<cmd>CornelisAuto<CR>")
      -- vim.keymap.set("n", "gd", "<cmd>CornelisGoToDefinition<CR>")
      -- vim.keymap.set("n", "[/", "<cmd>CornelisPrevGoal<CR>")
      -- vim.keymap.set("n", "]/", "<cmd>CornelisNextGoal<CR>")
      -- vim.keymap.set("n", "<C-A>", "<cmd>CornelisInc<CR>")
      -- vim.keymap.set("n", "<C-X>", "<cmd>CornelisDec<CR>")
    -- end
    -- vim.api.nvim_create_user_command('InitAgda', set_agda_kbs, {})
  end
}
