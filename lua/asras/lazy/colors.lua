return {
  {
    "folke/tokyonight.nvim",
    config = function()
      require("tokyonight").setup({
        -- your configuration comes here
        -- or leave it empty to use the default settings
        style = "night",                -- The theme comes in three styles, `storm`, `moon`, a darker variant `night` and `day`
        transparent = false,            -- Enable this to disable setting the background color
        terminal_colors = true,         -- Configure the colors used when opening a `:terminal` in Neovim
        styles = {
          -- Style to be applied to different syntax groups
          -- Value is any valid attr-list value for `:help nvim_set_hl`
          comments = { italic = false },
          keywords = { italic = false },
          -- Background styles. Can be "dark", "transparent" or "normal"
          sidebars = "dark",           -- style for sidebars, see below
          floats = "dark",             -- style for floating windows
        },
      })
      -- vim.cmd("colorscheme tokyonight")
    end
  },

  {
    "rose-pine/neovim",
    name = "rose-pine",
    config = function()
      require('rose-pine').setup({
        disable_background = false,
        styles = {
          bold = true,
          italic = false,
          transparency = false,
        },
      })

      -- vim.cmd("colorscheme rose-pine")
    end
  },

  {
    "savq/melange-nvim",
    name = "melange",
    config = function()
      vim.g.melange_style = "dark"
      -- vim.cmd("colorscheme melange")
    end
  },

  {
    "scottmckendry/cyberdream.nvim",
    name = "cyberdream",
    config = function()
      vim.cmd("colorscheme cyberdream")
    end
  },

  {
    "rebelot/kanagawa.nvim",
    name = "kanagawa",
    config = function()
      -- vim.cmd("colorscheme kanagawa")
    end
  },

  {
    "bluz71/vim-moonfly-colors",
    name = "moonfly",
    config = function()
      -- vim.cmd("colorscheme moonfly")
    end
  },
}
