local function simulate_rain()
  -- Create a new buffer
  local buf = vim.api.nvim_create_buf(false, true)

  -- Set buffer name
  vim.api.nvim_buf_set_name(buf, "Rain")

  -- Create a new window and set its options
  local width = math.floor(vim.o.columns * 0.8)
  local height = math.floor(vim.o.lines * 0.8)
  local win = vim.api.nvim_open_win(buf, true, {
    relative = "editor",
    width = width,
    height = height,
    col = math.floor((vim.o.columns - width) / 2),
    row = math.floor((vim.o.lines - height) / 2),
    style = "minimal",
    border = "rounded"
  })

  -- Initialize the rain array
  local rain = {}
  for i = 1, width do
    rain[i] = math.random(height)
  end

  -- Raindrop characters
  local drops = { '.', 'o', 'O' }

  -- Function to update the rain simulation
  local function update_rain()
    local lines = {}
    for _ = 1, height do
      table.insert(lines, string.rep(' ', width))
    end

    for col, row in ipairs(rain) do
      if row > 0 and row <= height then
        local line = lines[row]
        local drop = drops[math.random(#drops)]
        lines[row] = line:sub(1, col - 1) .. drop .. line:sub(col + 1)
      end

      rain[col] = math.min(rain[col] + 1, height + 1)
      if rain[col] == height + 1 and math.random() < 0.1 then
        rain[col] = 1
      end
      --if math.random() < 0.1 then
      --  rain[col] = 1
      --else
      --end
    end

    vim.api.nvim_buf_set_lines(buf, 0, -1, false, lines)

    if vim.api.nvim_win_is_valid(win) then
      vim.defer_fn(update_rain, 100)
    end
  end

  -- Start the rain simulation
  update_rain()

  -- Set up autocmd to close the window when cursor leaves
  vim.api.nvim_create_autocmd({ "WinLeave" }, {
    buffer = buf,
    callback = function()
      if vim.api.nvim_win_is_valid(win) then
        vim.api.nvim_win_close(win, true)
      end
    end
  })
end

-- Command to start the rain simulation
vim.api.nvim_create_user_command('RainSimulation', simulate_rain, {})
vim.api.nvim_create_user_command('RainSim', simulate_rain, {})
