local M = {}
M.chat_context = ""

local function get_selected_text()
  local start_pos = vim.fn.getpos("v")
  local end_pos = vim.fn.getpos(".")
  local line_start = start_pos[2]
  local line_end = end_pos[2]
  if line_start > line_end then
    line_start, line_end = line_end, line_start
  end

  local selected_text = table.concat(
    vim.api.nvim_buf_get_lines(0, line_start - 1, line_end, true),
    '\n'
  )

  return selected_text, line_start, line_end
end

function M.set_context()
  local content, _, _ = get_selected_text()
  M.chat_context = content
end

function M.clear_context()
  M.chat_context = ""
end

local function process_json_response(response_str)
  local start_index = string.find(response_str, "{")
  if not start_index then
    print("Failed to find JSON object in response")
    return
  end
  response_str = string.sub(response_str, start_index)
  local json_objects = vim.split(response_str, "}\n{", true)
  local processed_response = ""

  for i, json_str in ipairs(json_objects) do
    if i > 1 then
      json_str = "{" .. json_str
    end
    if i < #json_objects then
      json_str = json_str .. "}"
    end

    local success, decoded = pcall(vim.json.decode, json_str)
    if success and decoded.response then
        processed_response = processed_response .. decoded.response
    else
      print("Failed to decode JSON: " .. json_str)
    end
  end

  -- Go through each line, remove lines that start with ```
  local lines = vim.split(processed_response, '\n')
  local new_lines = {}
  for _, line in ipairs(lines) do
    if not string.match(line, '^```') then
      table.insert(new_lines, line)
    end
  end

  processed_response = table.concat(new_lines, '\n')

  return processed_response
end

local function create_float_window(content)
  local width = math.floor(vim.o.columns * 0.8)
  local height = math.floor(vim.o.lines * 0.8)

  local buf = vim.api.nvim_create_buf(false, true)
  local win = vim.api.nvim_open_win(buf, true, {
    relative = 'editor',
    width = width,
    height = height,
    col = math.floor((vim.o.columns - width) / 2),
    row = math.floor((vim.o.lines - height) / 2),
    style = 'minimal',
    border = 'rounded'
  })

  vim.api.nvim_buf_set_option(buf, 'modifiable', true)
  vim.api.nvim_buf_set_lines(buf, 0, -1, false, vim.split(content, '\n'))
  vim.api.nvim_win_set_option(win, 'wrap', true)

  return buf, win
end

local function handle_response(processed_response, line_start, line_end)
  local buf, win = create_float_window(processed_response)

  -- Set up keymaps for the floating window
  local opts = { noremap = true, silent = true, buffer = buf }
  vim.keymap.set('n', 'a', function()
    vim.api.nvim_win_close(win, true)
    M.chat_context = M.chat_context .. "\n" .. processed_response
    local lines = vim.split(processed_response, '\n')
    vim.api.nvim_buf_set_lines(0, line_start - 1, line_end, false, lines)
    -- vim.api.nvim_put(vim.split(processed_response, '\n'), 'c', true, true)
  end, opts)

  vim.keymap.set('n', 'r', function()
    vim.api.nvim_win_close(win, true)
  end, opts)

  -- Add instructions at the top of the buffer
  vim.api.nvim_buf_set_option(buf, 'modifiable', true)
  vim.api.nvim_buf_set_lines(buf, 0, 0, false, { "Press 'a' to accept and replace, or 'r' to reject and close", "" })
  vim.api.nvim_win_set_cursor(win, { 3, 0 }) -- Move cursor below the instructions
  vim.api.nvim_buf_set_option(buf, 'modifiable', false)
end


function M.send_to_ollama()
  local selected_text, line_start, line_end = get_selected_text()


  local message = selected_text .. "\n"
  if #M.chat_context ~= 0 then
    message = message .. "Here is relevant context that the file already contains:\n" .. M.chat_context .. "\n"
  end
  message = message .. "Only send back " .. vim.bo.filetype .. " code. Only return the new code, don't return the context."

  -- Replace with your actual Ollama endpoint URL
  local ollama_url = 'http://localhost:11434/api/generate'

  -- Prepare the request body
  local body = vim.fn.json_encode({
    model = "llama3.1", -- Replace with your desired model
    prompt = message
  })
  -- print("Sending: " .. body)

  -- Send the request to Ollama synchronously
  local response = vim.fn.system({
    'curl',
    '-X', 'POST',
    '-H', 'Content-Type: application/json',
    '-d', body,
    ollama_url
  })

  if vim.v.shell_error ~= 0 then
    print("Error: Failed to get response from Ollama")
    return
  end

  local processed_response = process_json_response(response)
  handle_response(processed_response, line_start, line_end)
end

-- Set up the command
vim.api.nvim_create_user_command('SendToOllama', M.send_to_ollama, { range = true })
vim.api.nvim_create_user_command('ClearOllamaContext', M.clear_context, {})

return M

