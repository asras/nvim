-- Show line numbers
vim.opt.nu = true
-- Show relative line numbers
vim.opt.relativenumber = true

-- Search
-- Dont highlight all matches
vim.opt.hlsearch = false
-- As you are typing, highlight next match
vim.opt.incsearch = true

-- Undo info
vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true


-- Tab
local indent_size = 2
vim.opt.tabstop = indent_size
vim.opt.softtabstop = indent_size
vim.opt.shiftwidth = indent_size
vim.opt.expandtab = true


-- Scroll
vim.opt.scrolloff = 8


-- Information column
vim.opt.signcolumn = "yes"



-- Filename stuff
vim.opt.isfname:append("@-@")


-- Swap file will be written to disk after 50 ms of no typing
vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.updatetime = 50

-- Enable 24-bit colors in TUI
vim.opt.termguicolors = true

-- Try to make the correct indentation when making a new line
vim.opt.smartindent = true


-- No wrap
vim.opt.wrap = false


-- Dont fmt in zig comon
vim.g.zig_fmt_autosave = 0
