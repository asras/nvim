local function get_comment_symbol()
    local filetype = vim.bo.filetype
    local comment_symbol = ""
    local comment_symbol_rep = " "
    local filename = vim.fn.expand("%")
    if filetype == "python" then
        comment_symbol = "#"
        comment_symbol_rep = "#"
    elseif filetype == "typescriptreact" then
        comment_symbol = "//"
        comment_symbol_rep = "\\/\\/"
    elseif filetype == "racket" then
        comment_symbol = ";"
        comment_symbol_rep = ";"
    elseif filetype == "haskell" then
        comment_symbol = "--"
        comment_symbol_rep = "--"
    elseif filename:match("agda$") then
        comment_symbol = "--"
        comment_symbol_rep = "--"
    elseif filetype == "lua" then
        -- Properly escape the comment symbol
        comment_symbol = "--"
        comment_symbol_rep = "--"
    elseif filetype == "ocaml" then
        comment_symbol = "(*"
        comment_symbol_rep = "(\\*"
    elseif filetype == "sh" then
        comment_symbol = "#"
        comment_symbol_rep = "#"
    elseif filetype == "mojo" then
        comment_symbol = "#"
        comment_symbol_rep = "#"
    else
        comment_symbol = "//"
        comment_symbol_rep = "\\/\\/"
    end

    return comment_symbol, comment_symbol_rep
end

local function toggle_comment_command()
    local comment_symbol, comment_symbol_rep = get_comment_symbol()

    local vstart = vim.fn.getpos('v') -- [bufnum, lnum, col, off]
    local vend = vim.fn.getpos('.')   --

    -- Handle reverse range selections
    local line_start = vstart[2]
    local line_end = vend[2]
    if line_start > line_end then
        line_start, line_end = line_end, line_start
    end

    -- Count number of comments
    local lines = vim.api.nvim_buf_get_lines(0, line_start - 1, line_end, true)
    local is_commented = false
    local comment_count = 0
    local not_comment_count = 0
    for _, line in ipairs(lines) do
        local trimmed = string.gsub(line, "^%s+", "")
        local find_result
        if vim.bo.filetype == "ocaml" then
            find_result = string.find(trimmed, "" .. comment_symbol, 1, true)
        else
            find_result = string.starts_with(trimmed, comment_symbol)
        end
        if find_result then
            comment_count = comment_count + 1
        else
            not_comment_count = not_comment_count + 1
        end
    end
    if comment_count > not_comment_count then
        is_commented = true
    end
    print("Commented: " .. tostring(is_commented))

    if vim.bo.filetype == "ocaml" then
        local range = line_start .. "," .. line_end
        if is_commented then
            local cmd = ":" .. range .. "s/(\\*\\s*//g"
            vim.cmd(cmd)
            cmd = ":" .. range .. "s/\\s*\\*)//g"
            vim.cmd(cmd)
        else 
            local cmd = ":" .. range .. "s/.*$/(* & *)/"
            vim.cmd(cmd)
        end

        return
    end

    if is_commented then
        local range = line_start .. "," .. line_end
        local cmd = ":" .. range .. "s/" .. comment_symbol_rep .. " //"
        vim.cmd(cmd)
    else
        local range = line_start .. "," .. line_end
        local cmd = ":" .. range .. "s/\\S/" .. comment_symbol_rep .. " &/"
        vim.cmd(cmd)
    end
end


vim.keymap.set("v", "<leader>c", toggle_comment_command, { noremap = true })


string.starts_with = function(str, start)
    return str:sub(1, #start) == start
end

local function toggle_comment_line()
    local comment_symbol, comment_symbol_rep = get_comment_symbol()

    local current_line = vim.api.nvim_get_current_line()
    local is_commented
    if vim.bo.filetype == "ocaml" then
        is_commented = string.find(current_line, "(*", 1, true)
    else
        local trimmed = string.gsub(current_line, "^%s+", "")
        is_commented = string.starts_with(trimmed, comment_symbol)
    end


    if vim.bo.filetype == "ocaml" then
        if is_commented then
            local cmd = ":s/(\\*\\s*//"
            vim.cmd(cmd)
            cmd = ":s/\\s*\\*)//"
            vim.cmd(cmd)
        else
            local cmd = ":s/.*$/(* & *)/"
            vim.cmd(cmd)
        end

        return
    end


    if is_commented then
        local cmd = ":s/" .. comment_symbol_rep .. " //"
        vim.cmd(cmd)
    else
        local cmd = ":s/\\S/" .. comment_symbol_rep .. " &/"
        vim.cmd(cmd)
    end
end


vim.keymap.set("n", "<leader>c", toggle_comment_line, { noremap = true })
