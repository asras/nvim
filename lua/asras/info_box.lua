local content = {}

local function open_float()
  -- Create a buffer for the floating window
  local buf = vim.api.nvim_create_buf(false, true)
  
  -- Set some example content
  vim.api.nvim_buf_set_lines(buf, 0, -1, true, content)

  -- Get the editor's width and height
  local width = vim.api.nvim_get_option("columns")
  local height = vim.api.nvim_get_option("lines")

  -- Calculate the window size
  local win_height = math.ceil(height * 0.8 - 4)
  local win_width = math.ceil(width * 0.8)

  -- Calculate the window position
  local row = math.ceil((height - win_height) / 2 - 1)
  local col = math.ceil((width - win_width) / 2)

  -- Set some options for the floating window
  local opts = {
    style = "minimal",
    relative = "editor",
    width = win_width,
    height = win_height,
    row = row,
    col = col,
    border = "rounded",
    title = "Info Box",
    title_pos = "center"

  }

  -- Create the floating window
  local win = vim.api.nvim_open_win(buf, true, opts)

  -- Set up a buffer-local keymapping for 'q' to close the window
  vim.api.nvim_buf_set_keymap(buf, 'n', 'q', '', {
    noremap = true,
    silent = true,
    callback = function()
      vim.api.nvim_win_close(win, true)
    end
  })

  vim.api.nvim_create_autocmd({"TextChanged", "TextChangedI"}, {
    buffer = buf,
    callback = function()
      content = vim.api.nvim_buf_get_lines(buf, 0, -1, false)
    end
  })
end


local function add_info_line(line)
  table.insert(content, line)
end


-- Return functions for use
return {
  open_float = open_float,
  add_info_line = add_info_line
}

-- Call the function to open the floating window
--vim.keymap.set("n", "<C-M>", open_float)
