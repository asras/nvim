-- Leader is space
vim.g.mapleader = " "

-- Open directory view
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Focus on this buffer
vim.keymap.set("n", "<leader>o", "<cmd>:only<CR>")

-- Move selected line up or down
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- On shift-j put line below on end of this line
vim.keymap.set("n", "J", "mzJ`z")


-- When going to next or previous search result, also focus screen on it
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")


-- Ctrl-C works like escape
vim.keymap.set("i", "<C-c>", "<Esc>")

-- Format
require("asras.format_remap")


-- Run cellular automaton
vim.keymap.set("n", "<leader>mr", "<cmd>CellularAutomaton make_it_rain<CR>")
vim.keymap.set("n", "<leader>gl", "<cmd>CellularAutomaton game_of_life<CR>")
vim.keymap.set("n", "<leader>mc", "<cmd>CellularAutomaton scramble<CR>")


-- Del,yank,pase to "a" register
vim.keymap.set("v", "<leader>d", "\"+d")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>p", "\"+p")


-- Locations
vim.keymap.set("n", "<leader>k", "<cmd>lnext<CR>zz")
vim.keymap.set("n", "<leader>j", "<cmd>lprev<CR>zz")


-- Move up and down a bunch
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

-- Run current file
local function run_this_file()
  local filetype = vim.bo.filetype
  if filetype == "python" then
    -- return "<cmd>write<CR><cmd>split<CR><cmd>wincmd j<CR><cmd>term python3 " .. vim.fn.expand("%") .. "<CR>"
    return "<cmd>write<CR><cmd>!python3 " .. vim.fn.expand("%") .. "<CR>"
  elseif filetype == "rust" then
    --return "<cmd>write<CR><cmd>split<CR><cmd>wincmd j<CR><cmd>term cargo r<CR>"
    return "<cmd>write<CR><cmd>!cargo r<CR>"
  elseif filetype == "cpp" or filetype == "c" then
    return "<cmd>write<CR><cmd>!./run.sh<CR>"
    --return "<cmd>write<CR><cmd>split<CR><cmd>wincmd j<CR><cmd>term ./run.sh<CR>"
  elseif filetype == "racket" then
    return "<cmd>write<CR><cmd>split<CR><cmd>wincmd j<CR><cmd>term racket " .. vim.fn.expand("%") .. "<CR>"
  elseif filetype == "haskell" then
    return "<cmd>write<CR><cmd>split<CR><cmd>wincmd j<CR><cmd>term cabal run .<CR>"
  elseif filetype == "zig" then
    return "<cmd>write<CR><cmd>split<CR><cmd>wincmd j<CR><cmd>term zig run " .. vim.fn.expand("%") .. "<CR>"
  end
end
vim.keymap.set("n", "<leader>q", run_this_file, { expr = true })
vim.keymap.set("n", "<leader>x", "<cmd>!chmod +x %<CR>", { silent = true })
-- Execute this file e.g. ./do_something.sh
-- vim.keymap.set("n", "<leader>r", function()
-- local cmd = "s/,/,\r/g"
-- vim.cmd(cmd);
-- end)


-- Comment
require("asras.comment_remap")


-- Rerun previous macro
vim.keymap.set("n", "-", "@@")


-- LSP keymaps
local opts = { remap = false }
vim.keymap.set("n", "gd", function() vim.lsp.buf.definition() end, opts)
vim.keymap.set("n", "K", function() vim.lsp.buf.hover() end, opts)
vim.keymap.set("n", "<leader>vws", function() vim.lsp.buf.workspace_symbol() end, opts)
vim.keymap.set("n", "<leader>vd", function() vim.diagnostic.open_float() end, opts)
vim.keymap.set("n", "[d", function() vim.diagnostic.goto_next() end, opts)
vim.keymap.set("n", "]d", function() vim.diagnostic.goto_prev() end, opts)
vim.keymap.set("n", "<leader>vca", function() vim.lsp.buf.code_action() end, opts)
vim.keymap.set("n", "<leader>vrr", function() vim.lsp.buf.references() end, opts)
vim.keymap.set("n", "<leader>vrn", function() vim.lsp.buf.rename() end, opts)
vim.keymap.set("i", "<C-h>", function() vim.lsp.buf.signature_help() end, opts)


-- Replace word under cursor
vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]], { noremap = true })



-- Ocaml stuff
vim.keymap.set("n", "<leader>t", "<cmd>:MerlinTypeOf<CR>")


-- Insert commit snippet
function CommitSnippet(value)
  local parts = vim.split(value.args, " ")
  local commit_parts = vim.split(parts[1], ",")
  local commit_type = commit_parts[1]
  local commit_scope = commit_parts[2]
  local message = table.concat(parts, " ", 2)
  local commit_scope_msg
  if commit_scope == nil then
    commit_scope_msg = ""
  else
    commit_scope_msg = "(" .. commit_scope .. ")"
  end
  vim.cmd("normal! i" .. commit_type .. commit_scope_msg .. ": " .. message)
end

vim.api.nvim_create_user_command("CommitSnippet", CommitSnippet, { nargs = 1 })
vim.keymap.set("n", "<leader>w", ":CommitSnippet ")


-- Read manual
vim.keymap.set("n", "<leader>h", "<C-]>")


-- Jupynium
-- vim.keymap.set("n", "<leader>gj", "<cmd>:JupyniumStartAndAttachToServer<CR>")
-- vim.keymap.set("n", "<leader>rj", "<cmd>:JupyniumStartSync<CR>")


-- Search visual selection on ctrl + S
vim.keymap.set("v", "<C-S>", "y/<C-R>\"<CR>")
vim.keymap.set("v", "<C-F>", "y/<C-R>\"")
vim.keymap.set("v", "<C-W>", "ymz/<C-R>\"<CR>'zcgn")



-- Info box stuff
local info = require("asras.info_box")
vim.keymap.set("n", "<C-T>", function()
  info.open_float()
end)

info.add_info_line("C-T                   ->    Open info box")
info.add_info_line("C-S                   ->    Search for visual selection")
info.add_info_line("C-F                   ->    Yank visual selection and paste into search")
info.add_info_line("C-W                   ->    Yank visual selection and cgn it")
info.add_info_line("(v)C-o                ->    Send to Ollama")
info.add_info_line("(v)C-p                ->    Set context for Ollama")
info.add_info_line(":ClearOllamaContext   ->    Clear context for Ollama")

info.add_info_line("i: <C-l>: λ")
info.add_info_line("i: <C-r>: →")
info.add_info_line("i: <C-a>: ∀")
info.add_info_line("i: <C-e>: ≡")

vim.keymap.set("i", "<C-q>", "≡⟨⟩")
info.add_info_line("i: <C-q>: ≡⟨⟩")

info.add_info_line("i: <C-V>u220E: ∎. QED symbol")
info.add_info_line("i: <C-V>u27e8: ⟨. Left angle bracket")
info.add_info_line("i: <C-V>u27e9: ⟩. Right angle bracket")
info.add_info_line("i: <C-V>u2238: ∸. Dot dash")

info.add_info_line("ℕ : In insert mode: <C-v>U2115")

info.add_info_line("")
info.add_info_line("")
info.add_info_line("")
info.add_info_line("Store and run command in attached terminal: <leader>ø")
info.add_info_line("Run stored command in attached terminal   : <leader>å")

-- Scratch pad
-- todo


-- Rain sim
require("asras.simulate_rain")

-- Growth sim
require("asras.growth_sim")


-- Insert special characters
vim.keymap.set("i", "<C-l>", "λ")
vim.keymap.set("i", "<C-r>", "→")
vim.keymap.set("i", "<C-a>", "∀")
vim.keymap.set("i", "<C-e>", "≡")


-- vim.keymap.set("i", "<C-r>", "→")
-- vim.keymap.set("i", "<C-h>", "Ђ")


-- TODO window
local edit_todo = require("asras.todo")
vim.api.nvim_create_user_command("Todo", edit_todo.EditTodoInFloatingWindow, {})
vim.keymap.set("n", "<leader>td", ":Todo<CR>")


-- Animate
require("asras.animate_buffer")


-- Custom Ollama
local ollama_module = require("asras.ollama")
vim.keymap.set("v", "<C-o>", ollama_module.send_to_ollama)
vim.keymap.set("v", "<C-p>", ollama_module.set_context)
-- vim.keymap.set("n", "<C-a>", ollama_module.clear_context)


-- Function viewver
local vfm = require("asras.view_funs")
vim.api.nvim_create_user_command("VF", vfm.view_functions, {})


-- Bash runner
-- require("asras.bash_runner")
-- vim.keymap.set("v", "<leader>rb", "<cmd>RunBashLines<CR>")


-- Split and goto def
vim.keymap.set("n", "sd", "<C-w><C-v><C-w><C-w>:lua vim.lsp.buf.definition()<CR>:normal zt<CR>")



-- Terminal setup
vim.keymap.set("t", "<C-n>", "<C-\\><C-n>")


local job_id
local term_buffer
vim.keymap.set("n", "<leader>tt", function()
  if term_buffer and vim.fn.bufexists(term_buffer) ~= 0 then
    vim.cmd.vnew()
    vim.cmd.wincmd("J")
    vim.api.nvim_win_set_height(0, 15)
    vim.cmd.buffer(term_buffer)
  else
    vim.cmd.vnew()
    vim.cmd.term()
    vim.cmd.wincmd("J")
    vim.api.nvim_win_set_height(0, 15)
    term_buffer = vim.api.nvim_buf_get_name(0)
    job_id = vim.bo.channel
  end
end)

local current_command = ""
vim.keymap.set("n", "<leader>ø", function()
  current_command = vim.fn.input("Command: ")
  if job_id then
    vim.fn.chansend(job_id, { current_command, "" })
  else
    vim.cmd.vnew()
    vim.cmd.term()
    vim.cmd.wincmd("J")
    vim.api.nvim_win_set_height(0, 15)
    term_buffer = vim.api.nvim_buf_get_name(0)
    job_id = vim.bo.channel
    vim.fn.chansend(job_id, { current_command, "" })
  end
end)
vim.keymap.set("n", "<leader>å", function()
  if current_command == "" then
    current_command = vim.fn.input("Command: ")
  end
  if job_id then
    vim.fn.chansend(job_id, { current_command, "" })
  else
    vim.cmd.vnew()
    vim.cmd.term()
    vim.cmd.wincmd("J")
    vim.api.nvim_win_set_height(0, 15)
    term_buffer = vim.api.nvim_buf_get_name(0)
    job_id = vim.bo.channel
    vim.fn.chansend(job_id, { current_command, "" })
  end
end)
