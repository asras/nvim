local api = vim.api
local timer = vim.loop.new_timer()

local M = {}

-- Global variables to track animation state
M.animating = false
M.paused = false
M.original_lines = {}
M.original_mapping = nil
M.original_p_mapping = nil

function M.animate_buffer()
  if M.animating then
    print("Animation already in progress")
    return
  end

  M.animating = true
  M.paused = false
  M.original_lines = api.nvim_buf_get_lines(0, 0, -1, false)
  local total_lines = #M.original_lines
  local current_line = 1
  local current_char = 1

  -- Clear the buffer
  api.nvim_buf_set_lines(0, 0, -1, false, {})

  -- Set Ctrl-C and P mappings
  M.set_animation_mappings()

  -- Set up the timer for the initial 3-second pause
  timer:start(3000, 0, vim.schedule_wrap(function()
    -- After the pause, start the animation
    local function animate_char()
      if not M.animating then
        M.cleanup()
        return
      end

      if M.paused then
        timer:start(100, 0, vim.schedule_wrap(animate_char))
        return
      end

      if current_line <= total_lines then
        local line = M.original_lines[current_line]
        -- Check for pause line
        if line:match("^// PAUSE") or line:match("^# PAUSE") or line:match("^-- PAUSE") then
          M.paused = true
          current_line = current_line + 1
          current_char = 1
          timer:start(100, 0, vim.schedule_wrap(animate_char))
          return
        end
        if current_char <= #line then
          -- Append the next character to the current line
          api.nvim_buf_set_lines(0, current_line - 1, current_line, false,
            {line:sub(1, current_char)})
          current_char = current_char + 1
        else
          -- Move to the next line
          current_line = current_line + 1
          current_char = 1
          -- Add a new line if we're not at the end
          if current_line <= total_lines then
            api.nvim_buf_set_lines(0, current_line - 1, current_line, false, {""})
          end
        end

        -- Center the view on the current line
        M.center_view(current_line)

        -- Calculate next delay with small random variation
        local next_delay = math.random(5, 20)

        timer:start(next_delay, 0, vim.schedule_wrap(animate_char))
      else
        M.cleanup()
      end
    end

    animate_char()
  end))
end

function M.finish_animation()
  if M.animating then
    M.animating = false
    M.paused = false
    api.nvim_buf_set_lines(0, 0, -1, false, M.original_lines)
    print("Animation finished")
    M.cleanup()
  else
    print("No animation in progress")
  end
end

function M.toggle_pause()
  if M.animating then
    M.paused = not M.paused
    print(M.paused and "Animation paused" or "Animation resumed")
  else
    print("No animation in progress")
  end
end

function M.set_animation_mappings()
  -- Store the original Ctrl-C mapping
  local ok, original_mapping = pcall(api.nvim_get_keymap, 'n', '<C-c>')
  M.original_mapping = ok and original_mapping[1] or nil

  -- Store the original P mapping
  local ok_p, original_p_mapping = pcall(api.nvim_get_keymap, 'n', 'p')
  M.original_p_mapping = ok_p and original_p_mapping[1] or nil

  -- Set new Ctrl-C mapping
  api.nvim_set_keymap('n', '<C-c>', ':lua require("asras.animate_buffer").finish_animation()<CR>', {noremap = true, silent = true})

  -- Set new P mapping
  api.nvim_set_keymap('n', 'p', ':lua require("asras.animate_buffer").toggle_pause()<CR>', {noremap = true, silent = true})
end

function M.restore_animation_mappings()
  if M.original_mapping then
    api.nvim_set_keymap('n', '<C-c>', M.original_mapping.rhs, {
      noremap = M.original_mapping.noremap == 1,
      silent = M.original_mapping.silent == 1,
    })
  else
    api.nvim_del_keymap('n', '<C-c>')
  end

  if M.original_p_mapping then
    api.nvim_set_keymap('n', 'p', M.original_p_mapping.rhs, {
      noremap = M.original_p_mapping.noremap == 1,
      silent = M.original_p_mapping.silent == 1,
    })
  else
    api.nvim_del_keymap('n', 'p')
  end
end

function M.cleanup()
  M.animating = false
  M.paused = false
  timer:stop()
  M.restore_animation_mappings()
end

function M.center_view(line)
  local buf_line_count = api.nvim_buf_line_count(0)
  -- Ensure we don't go past the last line of the buffer
  line = math.min(line, buf_line_count)
  -- Set cursor position
  pcall(api.nvim_win_set_cursor, 0, {line, 0})
  -- Center the view
  api.nvim_command('normal! zz')
end

-- Command to trigger the animation
api.nvim_create_user_command('AnimateBuffer', M.animate_buffer, {})

-- Command to finish the animation instantly
api.nvim_create_user_command('FinishAnimation', M.finish_animation, {})

-- Command to toggle pause/resume
api.nvim_create_user_command('TogglePauseAnimation', M.toggle_pause, {})

return M
