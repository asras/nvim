local api = vim.api
local width = 40
local height = 20

-- Create a new buffer and window
local function create_window()
    local buf = api.nvim_create_buf(false, true)
    local win = api.nvim_open_win(buf, true, {
        relative = 'editor',
        width = width,
        height = height,
        col = math.floor((vim.o.columns - width) / 2),
        row = math.floor((vim.o.lines - height) / 2),
        style = 'minimal',
        border = 'single'
    })
    return buf, win
end

-- Fill the buffer with '#' characters
local function fill_buffer(buf)
    local lines = {}
    for _ = 1, height do
        table.insert(lines, string.rep('#', width))
    end
    api.nvim_buf_set_lines(buf, 0, -1, false, lines)
end

-- Get neighboring positions
local function get_neighbors(row, col)
    local neighbors = {
        {row - 1, col}, {row + 1, col},
        {row, col - 1}, {row, col + 1}
    }
    return neighbors
end

-- Update the buffer
local function update_buffer(buf)
    local lines = api.nvim_buf_get_lines(buf, 0, -1, false)
    local changes = {}

    for row, line in ipairs(lines) do
        for col = 1, #line do
            if line:sub(col, col) == '*' then
                local neighbors = get_neighbors(row, col)
                local valid_neighbors = {}

                for _, pos in ipairs(neighbors) do
                    local n_row, n_col = pos[1], pos[2]
                    if n_row >= 1 and n_row <= #lines and n_col >= 1 and n_col <= #line then
                        if lines[n_row]:sub(n_col, n_col) == '#' then
                            table.insert(valid_neighbors, pos)
                        end
                    end
                end

                if #valid_neighbors > 0 then
                    local chosen = valid_neighbors[math.random(#valid_neighbors)]
                    table.insert(changes, {chosen[1], chosen[2]})
                end
            end
        end
    end

    for _, change in ipairs(changes) do
        local row, col = change[1], change[2]
        local line = lines[row]
        lines[row] = line:sub(1, col - 1) .. '*' .. line:sub(col + 1)
    end

    api.nvim_buf_set_lines(buf, 0, -1, false, lines)
end

-- Main function to run the plugin
local function run_plugin()
    local buf, win = create_window()
    fill_buffer(buf)

    -- Set up an autocommand to update the buffer every second
    api.nvim_create_autocmd("User", {
        pattern = "UpdateBuffer",
        callback = function()
            update_buffer(buf)
            vim.defer_fn(function()
                api.nvim_exec_autocmds("User", {pattern = "UpdateBuffer"})
            end, 1000)
        end
    })

    -- Trigger the first update
    api.nvim_exec_autocmds("User", {pattern = "UpdateBuffer"})
end

-- Create a command to start the plugin
vim.api.nvim_create_user_command("GrowthSim", run_plugin, {})
