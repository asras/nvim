require("asras")

vim.g.python3_host_prog = '/Users/asbjornrasmussen/.pyenv/shims/python3'


-- OCAML Stuff
-- local opam_share = vim.fn.substitute(vim.fn.system("opam var share"), '\n$', '', '')
-- vim.opt.rtp:prepend(opam_share .. "/merlin/vim")
-- vim.opt.rtp:prepend("/home/asras/.opam/default/share/ocp-indent/vim")
-- vim.fn.execute("helptags" .. opam_share .. "/merlin/vim/doc")
